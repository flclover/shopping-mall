import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
			 
const store = new Vuex.Store({
	state: {
		cartleng:0
	},
	mutations:{
		docart(state, payload){
			state.cartleng=payload
			if(state.cartleng>0){
				uni.setTabBarBadge({
					//给tabBar添加角标
					index: 2,
					text: String(state.cartleng)
				});
			}else{				
				uni.hideTabBarRedDot({
					index : 2,
				})
			}
		}
	},
	getters:{
		
	},
	actions:{
		cartleng(context,payload){				
			uni.$ajax('data/api.auth.shoopcart/get',{'uid':payload.uid},"POST").then((res)=>{				
				context.commit('docart',res.data.length)				
			})			
		}
	},
	modules:{
		
	}
	
})

export default store