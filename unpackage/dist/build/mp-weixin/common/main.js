(global["webpackJsonp"] = global["webpackJsonp"] || []).push([
  ["common/main"], {
    "0bd6": function (e, t, n) {
      "use strict";
      (function (e) {
        n("f2ee");
        var t = u(n("66fd")),
          o = (u(n("2f62")), u(n("8c87"))),
          a = u(n("1a3a")),
          r = u(n("a1b1")),
          c = u(n("4b70"));

        function u(e) {
          return e && e.__esModule ? e : {
            default: e
          }
        }

        function i(e, t) {
          var n = Object.keys(e);
          if (Object.getOwnPropertySymbols) {
            var o = Object.getOwnPropertySymbols(e);
            t && (o = o.filter((function (t) {
              return Object.getOwnPropertyDescriptor(e, t).enumerable
            }))), n.push.apply(n, o)
          }
          return n
        }

        function l(e) {
          for (var t = 1; t < arguments.length; t++) {
            var n = null != arguments[t] ? arguments[t] : {};
            t % 2 ? i(Object(n), !0).forEach((function (t) {
              f(e, t, n[t])
            })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : i(Object(n)).forEach((function (t) {
              Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t))
            }))
          }
          return e
        }

        function f(e, t, n) {
          return t in e ? Object.defineProperty(e, t, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
          }) : e[t] = n, e
        }
        var s = function () {
          n.e("pages/commponent/public/nodata").then(function () {
            return resolve(n("8fdf"))
          }.bind(null, n)).catch(n.oe)
        };
        t.default.prototype.$store = a.default, t.default.prototype.myrequest = r.default.request, t.default.component("nodata", s), t.default.config.productionTip = !1, t.default.prototype.$noMultipleClicks = c.default.noMultipleClicks, t.default.mixin({
          methods: {
            setData: function (e, t) {
              var n = this,
                o = function (e, t, n) {
                  return t = t.split("."), t.forEach((function (t) {
                    if (null === e[t] || void 0 === e[t]) {
                      var o = /^[0-9]+$/;
                      e[t] = o.test(n) ? [] : {}, e = e[t]
                    } else e = e[t]
                  })), e
                },
                a = function (e) {
                  return "function" == typeof e || !1
                };
              Object.keys(e).forEach((function (t) {
                var a, r, c = e[t];
                t = t.replace(/\]/g, "").replace(/\[/g, ".");
                var u = t.lastIndexOf("."); - 1 != u ? (r = t.slice(u + 1), a = o(n, t.slice(0, u), r)) : (r = t, a = n), a.$data && void 0 === a.$data[r] ? (Object.defineProperty(a, r, {
                  get: function () {
                    return a.$data[r]
                  },
                  set: function (e) {
                    a.$data[r] = e, n.$forceUpdate()
                  },
                  enumerable: !0,
                  configurable: !0
                }), a[r] = c) : n.$set(a, r, c)
              })), a(t) && this.$nextTick(t)
            }
          }
        }), o.default.mpType = "app";
        var d = new t.default(l({}, o.default));
        e(d).$mount()
      }).call(this, n("543d")["createApp"])
    },
    4461: function (e, t, n) {},
    6888: function (e, t, n) {
      "use strict";
      n.r(t);
      var o = n("cb18"),
        a = n.n(o);
      for (var r in o) "default" !== r && function (e) {
        n.d(t, e, (function () {
          return o[e]
        }))
      }(r);
      t["default"] = a.a
    },
    "8c87": function (e, t, n) {
      "use strict";
      n.r(t);
      var o = n("6888");
      for (var a in o) "default" !== a && function (e) {
        n.d(t, e, (function () {
          return o[e]
        }))
      }(a);
      n("9efb");
      var r, c, u, i, l = n("f0c5"),
        f = Object(l["a"])(o["default"], r, c, !1, null, null, null, !1, u, i);
      t["default"] = f.exports
    },
    "9efb": function (e, t, n) {
      "use strict";
      var o = n("4461"),
        a = n.n(o);
      a.a
    },
    cb18: function (e, t, n) {
      "use strict";
      (function (e) {
        Object.defineProperty(t, "__esModule", {
          value: !0
        }), t.default = void 0;
        var o = n("7119"),
          a = c(n("1001")),
          r = c(n("a1b1"));

        function c(e) {
          return e && e.__esModule ? e : {
            default: e
          }
        }
        var u = (0, o.getConfig)(),
          i = "";
        u && (i = u.color), e.$ajax = r.default.request;
        var l = {
          onLaunch: function () {
            var t = this,
              n = this;
            e.login({
              provider: "weixin",
              success: function (t) {
                console.log(t.code), n.myrequest("data/api.wxapp/session", {
                  code: t.code
                }).then((function (t) {
                  console.log("解密see", t), (0, o.setToken)(t.data.token.token), e.setStorageSync("uid", t.data.id), e.setStorageSync("openid1", t.data.openid1)
                }))
              }
            }), e.getSetting({
              success: function (n) {
                n.authSetting["scope.userInfo"] && e.getUserInfo({
                  success: function (e) {
                    t.globalData.userInfo = e.userInfo, t.userInfoReadyCallback && t.userInfoReadyCallback(e)
                  }
                })
              }
            }), e.getSystemInfo({
              complete: function (e) {
                t.globalData.statusHeight = e.statusBarHeight, "ios" == e.platform ? t.globalData.toBar = 44 : "android" == e.platform ? t.globalData.toBar = 48 : t.globalData.toBar = 44
              }
            });
            var a = (0, o.getConfig)();
            if (a && "" !== a.color) {
              e.setTabBarStyle({
                selectedColor: a.color
              });
              var r = a.tabList;
              if (a.tabList)
                for (var c = 0; c < r.length; c++) {
                  var u = r[c];
                  e.setTabBarItem({
                    index: c,
                    selectedIconPath: u
                  })
                }
            } else {
              var i = {
                color: "#1cbbb4",
                name: "cyan"
              };
              (0, o.setConfig)(i), e.setTabBarStyle({
                selectedColor: "#1cbbb4"
              })
            }
            var l = (0, o.getCart)(),
              f = e.getStorageSync("phone"),
              s = "";
            l && f && (s = l.length, e.setTabBarBadge({
              index: 2,
              text: String(s)
            }))
          },
          globalData: {
            userInfo: null,
            statusHeight: "20",
            toBar: "44",
            newColor: i || "#1cbbb4",
            config: a.default.themeList
          },
          created: function () {
            var t = e.getStorageSync("uid"),
              n = e.getStorageSync("phone");
            n && this.$store.dispatch({
              type: "cartleng",
              uid: t
            })
          },
          methods: {}
        };
        t.default = l
      }).call(this, n("543d")["default"])
    }
  },
  [
    ["0bd6", "common/runtime", "common/vendor"]
  ]
]);