(global["webpackJsonp"] = global["webpackJsonp"] || []).push([
  ["pages/login/register"], {
    4029: function (t, e, n) {
      "use strict";
      (function (t) {
        Object.defineProperty(e, "__esModule", {
          value: !0
        }), e.default = void 0;
        var o = n("7119"),
          i = {
            data: function () {
              return {
                isCanUse: t.getStorageSync("isCanUse"),
                nickName: "",
                avatarUrl: "",
                bgImg: ["https://6d61-matchbox-79a395-1302390714.tcb.qcloud.la/matchbox/img_flower_4.jpg", "https://6d61-matchbox-79a395-1302390714.tcb.qcloud.la/matchbox/img_flower_1.jpg", "https://6d61-matchbox-79a395-1302390714.tcb.qcloud.la/matchbox/img_flower_3.jpg", "https://6d61-matchbox-79a395-1302390714.tcb.qcloud.la/matchbox/img_flower_2.jpg"],
                imgTime: "",
                imgIndex: 0,
                codeName: "验证码",
                isCode: !0,
                tel: "",
                smscode: "",
                username: "",
                maile: "",
                pass_maile: ""
              }
            },
            props: {},
            onLoad: function (t) {
              this.wxlogin()
            },
            onReady: function () {},
            onShow: function () {
              this.setbImg()
            },
            onHide: function () {
              clearInterval(this.imgTime)
            },
            onUnload: function () {
              clearInterval(this.imgTime)
            },
            onPullDownRefresh: function () {},
            onReachBottom: function () {},
            onShareAppMessage: function () {},
            methods: {
              getUserInfo: function () {
                console.log("点了");
                var e = this;
                t.getUserInfo({
                  provider: "weixin",
                  success: function (n) {
                    e.setData({
                      nickName: n.userInfo.nickName,
                      avatarUrl: n.userInfo.avatarUrl
                    });
                    var i = (new Date).getTime();
                    (0, o.setToken)(i), (0, o.setUserInfo)(n.userInfo);
                    try {
                      t.setStorageSync("isCanUse", 1), t.switchTab({
                        url: "/pages/views/tabBar/home"
                      })
                    } catch (a) {}
                  },
                  fail: function (t) {}
                })
              },
              setbImg: function () {
                clearInterval(this.imgTime);
                var t = this,
                  e = setInterval((function () {
                    var e = t.imgIndex + 1;
                    e >= t.bgImg.length && (e = 0), t.setData({
                      imgIndex: e
                    })
                  }), 15e3);
                this.setData({
                  imgTime: e
                })
              },
              onlogin: function () {
                var e = "注册失败";
                this.myrequest("/data/api.login/register", {
                  phone: this.tel,
                  username: this.username,
                  password: this.smscode
                }).then((function (n) {
                  1 == n.code && (e = "注册成功,跳转登录界面", setTimeout((function () {
                    t.navigateBack(-1)
                  }), 1500))
                })), t.showLoading({
                  title: "注册中..."
                }), setTimeout((function () {
                  t.hideLoading(), t.showToast({
                    title: e
                  })
                }), 500)
              },
              onregister: function () {
                t.navigateTo({
                  url: "/pages/login/login"
                })
              },
              getCode: function () {
                if (0 != this.isCode) {
                  if ("" != this.tel) return /^[A-Za-z]\w{5,17}@(163\.com|126\.com|yeach\.net)$/.test(this.maile) ? void console.log("发送邮箱") : (t.showToast({
                    title: "请填写正确邮箱号",
                    icon: "none"
                  }), !1);
                  t.showToast({
                    title: "请输入手机号",
                    icon: "none"
                  })
                }
              },
              getPhoneCode: function () {
                var e = "",
                  n = 120,
                  o = this;
                1 == o.isCode && (t.showToast({
                  title: "验证码发送成功~",
                  icon: "none"
                }), clearInterval(e), setInterval((function () {
                  n >= 1 ? (n--, o.codeName = n + "秒重试", o.isCode = !1) : (o.isCode = !0, o.codeName = "验证码", clearInterval(e))
                }), 1e3))
              },
              wxlogin: function () {
                t.login({
                  provider: "weixin",
                  success: function (t) {}
                })
              },
              onAuthorize: function () {
                t.showToast({
                  title: "对接你的公众号登录方法",
                  icon: "none"
                })
              }
            }
          };
        e.default = i
      }).call(this, n("543d")["default"])
    },
    "60fb": function (t, e, n) {
      "use strict";
      var o;
      n.d(e, "b", (function () {
        return i
      })), n.d(e, "c", (function () {
        return a
      })), n.d(e, "a", (function () {
        return o
      }));
      var i = function () {
          var t = this,
            e = t.$createElement;
          t._self._c
        },
        a = []
    },
    "916f": function (t, e, n) {
      "use strict";
      n.r(e);
      var o = n("4029"),
        i = n.n(o);
      for (var a in o) "default" !== a && function (t) {
        n.d(e, t, (function () {
          return o[t]
        }))
      }(a);
      e["default"] = i.a
    },
    "97d8": function (t, e, n) {},
    d8e3: function (t, e, n) {
      "use strict";
      n.r(e);
      var o = n("60fb"),
        i = n("916f");
      for (var a in i) "default" !== a && function (t) {
        n.d(e, t, (function () {
          return i[t]
        }))
      }(a);
      n("f0f1");
      var s, c = n("f0c5"),
        r = Object(c["a"])(i["default"], o["b"], o["c"], !1, null, "a81f92b4", null, !1, o["a"], s);
      e["default"] = r.exports
    },
    dc9e: function (t, e, n) {
      "use strict";
      (function (t) {
        n("f2ee");
        o(n("66fd"));
        var e = o(n("d8e3"));

        function o(t) {
          return t && t.__esModule ? t : {
            default: t
          }
        }
        t(e.default)
      }).call(this, n("543d")["createPage"])
    },
    f0f1: function (t, e, n) {
      "use strict";
      var o = n("97d8"),
        i = n.n(o);
      i.a
    }
  },
  [
    ["dc9e", "common/runtime", "common/vendor"]
  ]
]);