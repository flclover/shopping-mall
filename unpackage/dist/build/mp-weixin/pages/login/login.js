(global["webpackJsonp"] = global["webpackJsonp"] || []).push([
  ["pages/login/login"], {
    "13f2": function (e, t, n) {
      "use strict";
      (function (e) {
        Object.defineProperty(t, "__esModule", {
          value: !0
        }), t.default = void 0;
        var o = n("7119"),
          i = {
            data: function () {
              return {
                isCanUse: e.getStorageSync("isCanUse"),
                nickName: "",
                avatarUrl: "",
                bgImg: ["https://6d61-matchbox-79a395-1302390714.tcb.qcloud.la/matchbox/img_flower_4.jpg", "https://6d61-matchbox-79a395-1302390714.tcb.qcloud.la/matchbox/img_flower_1.jpg", "https://6d61-matchbox-79a395-1302390714.tcb.qcloud.la/matchbox/img_flower_3.jpg", "https://6d61-matchbox-79a395-1302390714.tcb.qcloud.la/matchbox/img_flower_2.jpg"],
                imgTime: "",
                imgIndex: 0,
                codeName: "验证码",
                isCode: !0,
                tel: "",
                smscode: ""
              }
            },
            props: {},
            onLoad: function (e) {
              this.wxlogin()
            },
            onReady: function () {},
            onShow: function () {
              this.setbImg()
            },
            onHide: function () {
              clearInterval(this.imgTime)
            },
            onUnload: function () {
              clearInterval(this.imgTime)
            },
            onPullDownRefresh: function () {},
            onReachBottom: function () {},
            onShareAppMessage: function () {},
            methods: {
              getUserInfo: function () {
                console.log("点了");
                var t = this;
                e.getUserInfo({
                  provider: "weixin",
                  success: function (n) {
                    t.setData({
                      nickName: n.userInfo.nickName,
                      avatarUrl: n.userInfo.avatarUrl
                    });
                    var i = (new Date).getTime();
                    (0, o.setToken)(i), (0, o.setUserInfo)(n.userInfo);
                    try {
                      e.setStorageSync("isCanUse", 1), e.switchTab({
                        url: "/pages/views/tabBar/home"
                      })
                    } catch (a) {}
                  },
                  fail: function (e) {}
                })
              },
              setbImg: function () {
                clearInterval(this.imgTime);
                var e = this,
                  t = setInterval((function () {
                    var t = e.imgIndex + 1;
                    t >= e.bgImg.length && (t = 0), e.setData({
                      imgIndex: t
                    })
                  }), 15e3);
                this.setData({
                  imgTime: t
                })
              },
              onlogin: function () {
                var t, n = this;
                this.myrequest("data/api.login/in", {
                  phone: this.tel,
                  password: this.smscode
                }).then((function (i) {
                  (new Date).getTime();
                  if (1 == i.code) {
                    t = {
                      uid: i.data.id,
                      token: i.data.token.token,
                      expire: i.data.token.expire,
                      nickName: i.data.username
                    }, (0, o.setUserInfo)(t), (0, o.setToken)(i.data.token.token), "登录成功";
                    var a = n;
                    e.login({
                      provider: "weixin",
                      success: function (e) {
                        a.myrequest("data/api.wxapp/session", {
                          code: e.code
                        }).then((function (e) {
                          console.log("LOGIN______CODE______::::", e), t = {
                            uid: i.data.id,
                            token: i.data.token.token,
                            expire: i.data.token.expire,
                            nickName: i.data.username,
                            openid1: e.data.openid1
                          }, (0, o.setUserInfo)(t)
                        }))
                      }
                    }), setTimeout((function () {
                      e.navigateBack(-1)
                    }), 1500)
                  }
                  e.showLoading({
                    title: "登录中..."
                  }), setTimeout((function () {
                    e.hideLoading(), e.showToast({
                      title: i.info,
                      icon: "none"
                    })
                  }), 500)
                }))
              },
              onregister: function () {
                e.navigateTo({
                  url: "/pages/login/register"
                })
              },
              getCode: function () {
                if (0 != this.isCode) {
                  if ("" != this.tel) return /^1(3|4|5|6|7|8|9)\d{9}$/.test(this.tel) ? void this.getPhoneCode() : (e.showToast({
                    title: "请填写正确手机号码",
                    icon: "none"
                  }), !1);
                  e.showToast({
                    title: "请输入手机号",
                    icon: "none"
                  })
                }
              },
              getPhoneCode: function () {
                var t = "",
                  n = 120,
                  o = this;
                1 == o.isCode && (e.showToast({
                  title: "验证码发送成功~",
                  icon: "none"
                }), clearInterval(t), setInterval((function () {
                  n >= 1 ? (n--, o.codeName = n + "秒重试", o.isCode = !1) : (o.isCode = !0, o.codeName = "验证码", clearInterval(t))
                }), 1e3))
              },
              wxlogin: function () {},
              onAuthorize: function () {
                e.showToast({
                  title: "对接你的公众号登录方法",
                  icon: "none"
                })
              }
            }
          };
        t.default = i
      }).call(this, n("543d")["default"])
    },
    "2f85": function (e, t, n) {
      "use strict";
      var o = n("59df"),
        i = n.n(o);
      i.a
    },
    3817: function (e, t, n) {
      "use strict";
      n.r(t);
      var o = n("13f2"),
        i = n.n(o);
      for (var a in o) "default" !== a && function (e) {
        n.d(t, e, (function () {
          return o[e]
        }))
      }(a);
      t["default"] = i.a
    },
    "4f3f": function (e, t, n) {
      "use strict";
      (function (e) {
        n("f2ee");
        o(n("66fd"));
        var t = o(n("de26"));

        function o(e) {
          return e && e.__esModule ? e : {
            default: e
          }
        }
        e(t.default)
      }).call(this, n("543d")["createPage"])
    },
    "59df": function (e, t, n) {},
    a4f2: function (e, t, n) {
      "use strict";
      var o;
      n.d(t, "b", (function () {
        return i
      })), n.d(t, "c", (function () {
        return a
      })), n.d(t, "a", (function () {
        return o
      }));
      var i = function () {
          var e = this,
            t = e.$createElement;
          e._self._c
        },
        a = []
    },
    de26: function (e, t, n) {
      "use strict";
      n.r(t);
      var o = n("a4f2"),
        i = n("3817");
      for (var a in i) "default" !== a && function (e) {
        n.d(t, e, (function () {
          return i[e]
        }))
      }(a);
      n("2f85");
      var s, c = n("f0c5"),
        r = Object(c["a"])(i["default"], o["b"], o["c"], !1, null, "fb38d3a4", null, !1, o["a"], s);
      t["default"] = r.exports
    }
  },
  [
    ["4f3f", "common/runtime", "common/vendor"]
  ]
]);